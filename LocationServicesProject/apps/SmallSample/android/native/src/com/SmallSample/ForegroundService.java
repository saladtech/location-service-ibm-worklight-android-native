/*
    Licensed Materials - Property of IBM
    5725-I43 (C) Copyright IBM Corp. 2013. All Rights Reserved.
    US Government Users Restricted Rights - Use, duplication or
    disclosure restricted by GSA ADP Schedule Contract with IBM Corp.
*/

package com.SmallSample;

public class ForegroundService extends com.worklight.androidgap.WLForegroundService{
	//Nothing to do here...
}