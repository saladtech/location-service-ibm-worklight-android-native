
/* JavaScript content from js/Sample.js in folder common */
/*
* Licensed Materials - Property of IBM
* 5725-I43 (C) Copyright IBM Corp. 2006, 2013. All Rights Reserved.
* US Government Users Restricted Rights - Use, duplication or
* disclosure restricted by GSA ADP Schedule Contract with IBM Corp.
*/

function wlCommonInit(){

	/*
	 * Application is started in offline mode as defined by a connectOnStartup property in initOptions.js file.
	 * In order to begin communicating with Worklight Server you need to either:
	 * 
	 * 1. Change connectOnStartup property in initOptions.js to true. 
	 *    This will make Worklight framework automatically attempt to connect to Worklight Server as a part of application start-up.
	 *    Keep in mind - this may increase application start-up time.
	 *    
	 * 2. Use WL.Client.connect() API once connectivity to a Worklight Server is required. 
	 *    This API needs to be called only once, before any other WL.Client methods that communicate with the Worklight Server.
	 *    Don't forget to specify and implement onSuccess and onFailure callback functions for WL.Client.connect(), e.g:
	 *    
	 *    WL.Client.connect({
	 *    		onSuccess: onConnectSuccess,
	 *    		onFailure: onConnectFailure
	 *    });
	 *     
	 */
	
	
	// Common initialization code goes here	
}


var policy;
var triggers;

function geoFailure(positionError) {
	// check code and perform logic
}

function wifiFailure(wifiError) {
	// check code and perform logic
}

var acquisitionFailure = {
		Geo:	geoFailure,
		Wifi:	wifiFailure
};

function startLiveTracking() {
	policy.Geo = WL.Device.Geo.Profiles.LiveTracking();
	WL.Device.startAcquisition(policy, triggers, acquisitionFailure);
}

function startWifiTracking() {
	policy.Wifi = {
			interval: 10000,
			accessPointsFilters: [{SSID: 'InnovationHotel'}]
	};
	WL.Device.startAcquisition(policy, triggers, acquisitionFailure);			 
}

function arrived() {
	WL.Device.stopAcquisition();
	WL.App.setKeepAliveInBackground(false);
}

function pushReceived(props, payload){ 
	var hotel = payload.coords;  

	var triggers = { 
			Geo: {
				At4k: {
					type: 'Enter',
					circle: {longitude : hotel.longitude, latitude: hotel.latitude, radius: 4000},
					callback: startLiveTracking
				},
				At2k: {
					type: 'Enter',
					circle: {longitude : hotel.longitude, latitude: hotel.latitude, radius: 2000},
					eventToTransmit: { event: {name: 'at2k'} },
					callback: startWifiTracking
				},
				At100: {
					type: 'Enter',
					circle: {longitude : hotel.longitude, latitude: hotel.latitude, radius: 100},
					callback: arrived,                   
					eventToTransmit: { event: {name: 'arrived'} }
				},
			},
			
			Wifi: {
				Arrived: {
					type: 'Enter',
					areaAccessPoints: [{SSID: 'InnovationHotel'}],
					callback: arrived,
					eventToTransmit: { event: {name: 'arrived'} }
				}
			}
	  };	

	WL.Client.setEventTransmissionPolicy({interval: 0});

	policy = { Geo: WL.Device.Geo.Profiles.PowerSaving() };
	WL.Device.startAcquisition(policy, triggers, acquisitionFailure);
	
	WL.App.setKeepAliveInBackground(true); 
}
/* JavaScript content from js/Sample.js in folder android */
/*
 *  Licensed Materials - Property of IBM
 *  5725-I43 (C) Copyright IBM Corp. 2011, 2013. All Rights Reserved.
 *  US Government Users Restricted Rights - Use, duplication or
 *  disclosure restricted by GSA ADP Schedule Contract with IBM Corp.
 */

// This method is invoked after loading the main HTML and successful initialization of the Worklight runtime.
function wlEnvInit(){
    wlCommonInit();
    // Environment initialization code goes here
}