/*
 *  Licensed Materials - Property of IBM
 *  5725-I43 (C) Copyright IBM Corp. 2011, 2013. All Rights Reserved.
 *  US Government Users Restricted Rights - Use, duplication or
 *  disclosure restricted by GSA ADP Schedule Contract with IBM Corp.
 */


var notificationText = 'Message from Innovation Hotel';

function push(user, notification) {
  WL.Server.notifyAllDevices(WL.Server.getUserNotificationSubscription('Hotel.Arriving', user), notification);
}

function planeArrived(userId) { 
  push(userId, WL.Server.createDefaultNotification(notificationText, 1,	{status: 'welcome', coords: getHotelGeoLocation() } ));
}

function guestNearby() {
    if (isPlatinumUser(userId))
        push(getShiftManagerId(),  	WL.Server.createDefaultNotification('Customer arriving soon',  1, {status: 'nearby', id: userId} ));
}

function guestArrived() {
	if (isPlatinumUser(userId))
		push(getShiftManagerId(), WL.Server.createDefaultNotification('Customer arriving', 1, {status: 'arriving', id: userId} ));
}

function getHotelGeoLocation() {
	// call a back-end system to get the actual data, or substitute a hard-coded value
	// demo purposes:
	return {
		longitude: 0,
		latitude: 0
	};
}


function isPlatinumUser(userId) {
	// call a back-end system...
	// demo purposes:
	return true;
}

function getShiftManagerId() {
	// call a back-end system...
	// demo purposes:
	return "nightManager";
}

WL.Server.setEventHandlers([
                            WL.Server.createEventHandler({name: 'at2k'}, guestNearby),
                            WL.Server.createEventHandler({name: 'arrived'}, guestArrived)
                           ]);
